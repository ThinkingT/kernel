/*************************************************************************
	> File Name: signal.c
	> Author: Thinking
	> Mail: program_code@sohu.com 
	> Created Time: Wed 16 Jun 2021 05:45:46 AM PDT
 ************************************************************************/

#include <linux/lsm_hooks.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kprobes.h>
#include <linux/kallsyms.h>
#include <linux/kthread.h>


struct task_struct* result;

int hook_process_func(void* args)
{
	printk("pid ==>%d\n",current->pid);
	return 0;
}

static __init int main_init(void)
{

	result = kthread_create_on_node(hook_process_func, NULL, -1, "ktconnode.c");

	printk("new pid ==>%d\n", result->pid);
	
	wake_up_process(result);

	printk("wake pid ==>%d\n",current->pid);

	return 0;
}
static __exit void main_exit(void)
{

}

module_init(main_init); 
module_exit(main_exit); 

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Kernel Module");
MODULE_AUTHOR("lee");
