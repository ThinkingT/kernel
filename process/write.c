/*************************************************************************
	> File Name: test.c
	> Author: Thinking
	> Mail: program_code@sohu.com 
	> Created Time: Wed 16 Jun 2021 11:20:50 PM PDT
 ************************************************************************/

#include<stdio.h>

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/select.h>


#define DATA_NUM    (32)

int main(int argc, char *argv[])
{
	int fd, i;
	int r_len, w_len;
	fd_set fdset;
	char buf[DATA_NUM]="hello world";
	fd = open("/dev/hello", O_RDWR);
	printf("%d\r\n",fd);
	if(-1 == fd) {
		perror("open file error\r\n");
		return -1;
	}
	else {
		printf("open successe\r\n");
	}

	sleep(5);
	w_len = write(fd, buf, DATA_NUM);
	printf("write len %d\n",w_len);
	close(fd);

	return 0;
}
