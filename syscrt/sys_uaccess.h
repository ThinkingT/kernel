#ifndef _SYS_UACCESS_H_
#define _SYS_UACCESS_H_

#include <linux/fs.h>

#define SYS_OPEN_UACCESS_SUCCESS	0
#define SYS_FAILD_UACCESS			1

int sys_open_uaccess(mm_segment_t* fs);
int sys_close_uaccess(mm_segment_t* fs);

#endif

