/*************************************************************************
	> File Name: common.c
	> Author: Thinking
	> Mail: program_code@sohu.com 
	> Created Time: Sun 17 Oct 2021 06:56:35 PM CST
 ************************************************************************/

#include "sys_uaccess.h"
#include <linux/version.h>
#include <linux/uaccess.h>

int sys_open_uaccess(mm_segment_t* fs)
{

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,0,0)
	*fs = get_fs();
	set_fs(get_ds());
#elif LINUX_VERSION_CODE < KERNEL_VERSION(5,10,0)
	*fs = get_fs();
	set_fs(KERNEL_DS);
#else
	*fs = force_uaccess_begin();
#endif

	return SYS_OPEN_UACCESS_SUCCESS;
}


int sys_close_uaccess(mm_segment_t* fs)
{
	
#if LINUX_VERSION_CODE < KERNEL_VERSION(5,10,0)
	set_fs(*fs);
#else
	force_uaccess_end(*fs);
#endif
	return SYS_OPEN_UACCESS_SUCCESS;
}


