/*************************************************************************
	> File Name: signal.c
	> Author: Thinking
	> Mail: program_code@sohu.com 
	> Created Time: Wed 16 Jun 2021 05:45:46 AM PDT
 ************************************************************************/

#include <linux/lsm_hooks.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/mount.h>
#include <linux/security_ops.h>



int test_sb_mount(const char *dev_name, const struct path *path, const char *type, unsigned long flags, void *data)
{
	pr_alert("%s mounted at %s\n", dev_name?dev_name:"none", path->dentry->d_iname);
	return 0;
}
int test_sb_umount(struct vfsmount *mnt, int flags)
{
	pr_alert("%s umounted\n", mnt->mnt_root->d_iname);
	return 0;
}
int test_sb_capget(struct task_struct * target, kernel_cap_t * effective,
		kernel_cap_t * inheritable, kernel_cap_t * permitted)
{
	pr_alert("capget");
	return 0;
}



static struct security_operations test_ops = {
	.name = "test",
	.sb_mount = test_sb_mount,
	.sb_umount= test_sb_umount,
	.capget = test_sb_capget
};

static int __init kyd_init(void)
{
	int ret = 0;
	ret = security_add_external_ops(&test_ops);
	if (ret < 0)
		return ret;

	return 0;
}

void __exit kyd_exit(void)
{
	
	security_del_external_ops(&test_ops);	
    	return;
}
module_init(kyd_init);
module_exit(kyd_exit);
MODULE_LICENSE("GPL");
