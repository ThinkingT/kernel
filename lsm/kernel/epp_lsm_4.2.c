#include "ak_internal.h"
#include "ak_probe.h"
#include "lsm_public.h"
/* For exporting variables and functions. */
struct ak_security_exports ak_sec_exports;
/* Members are updated by loadable kernel module. */
struct ak_security_operations ak_security_ops;

int check_procname(char *procname)
{
    if (0 == strcmp(procname, "/usr/bin/vi") ||
        0 == strcmp(procname, "/usr/bin/vim") ||
        0 == strcmp(procname, "/usr/bin/touch") ||
        0 == strcmp(procname, "/usr/bin/cat") ||
        0 == strcmp(procname, "/usr/bin/chmod") ||
        0 == strcmp(procname, "/usr/bin/rm") ||
        0 == strcmp(procname, "/usr/bin/ls"))
     {
        return 1;
     }
     else
        return 0;
}
int check_filename(char *filename)
{
    if (strstr(filename, "/share/swf/"))
    {
        return 1;
    }
    else
        return 0;
}

int ak360_inode_create(struct inode *dir, struct dentry *dentry, umode_t mode)
{
    char *procname = NULL;
    //struct vfsmount *vfsmnt = sec_vfsmount(dentry); 
    struct path path = {
        .dentry = dentry,
        .mnt = NULL,
    };
    
    char *filename = ak_get_realpath(&path);
	if (filename)
	{
	    if (check_filename(filename))
		    printk("%s filename = %s\n", __FUNCTION__, filename);
		kfree(filename);
	}
	else
    {
        printk("%s get filename failed.\n", __FUNCTION__);
    }
	procname = (char *)ak_get_procname();
    if (procname)
    {
        if (check_procname(procname))
        printk("%s procname = %s\n", __FUNCTION__, procname);
        kfree(procname);
    }

    return 0;
}

int ak360_inode_mkdir(struct inode *dir, struct dentry *dentry,  umode_t mode)
{
    char *procname = NULL;
        
    struct path path = {
        .dentry = dentry,
        .mnt = NULL,
    };
    
    char *filename = ak_get_realpath(&path);
    if (filename)
    {
        if (check_filename(filename))
            printk("%s filename = %s\n", __FUNCTION__, filename);
        kfree(filename);
    }

    procname = (char *)ak_get_procname();
    if (procname)
    {
        if (check_procname(procname))
        printk("%s procname = %s\n", __FUNCTION__, procname);
        kfree(procname);
    }

    return 0;
}

int ak360_inode_rmdir(struct inode *dir,  struct dentry *dentry)
{
    char *procname = NULL;
    struct path path = {
        .dentry = dentry,
        .mnt = NULL,
    };
    
    char *filename = ak_get_realpath(&path);
	if (filename)
	{
	    if (check_filename(filename))
		    printk("%s filename = %s\n", __FUNCTION__, filename);
		kfree(filename);
	}

	procname = (char *)ak_get_procname();
    if (procname)
    {
        if (check_procname(procname))
        printk("%s procname = %s\n", __FUNCTION__, procname);
        kfree(procname);
    }

    return 0;
}
int ak360_inode_link(struct dentry *old_dentry, struct inode *dir, struct dentry *new_dentry)
{
    char *procname = NULL;
    char *oldfilename = NULL;
    char *newfilename = NULL;
    
    struct path oldpath = {
        .dentry = old_dentry,
        .mnt = NULL,
    };

    struct path newpath = {
        .dentry = new_dentry,
        .mnt = NULL,
    };
    
    oldfilename = ak_get_realpath(&oldpath);
	if (oldfilename)
	{
	    if (check_filename(oldfilename))
		    printk("%s oldfilename = %s\n", __FUNCTION__, oldfilename);
		kfree(oldfilename);
	}

	newfilename = ak_get_realpath(&newpath);
	if (newfilename)
	{
	    if (check_filename(newfilename))
		    printk("%s newfilename = %s\n", __FUNCTION__, newfilename);
		kfree(newfilename);
	}
	
	procname = (char *)ak_get_procname();
    if (procname)
    {
        if (check_procname(procname))
        printk("%s procname = %s\n", __FUNCTION__, procname);
        kfree(procname);
    }

    return 0;
}
int ak360_inode_unlink(struct inode *dir, struct dentry *dentry)
{
    char *procname = NULL;
    struct path path = {
        .dentry = dentry,
        .mnt = NULL,
    };
    
    char *filename = ak_get_realpath(&path);
	if (filename) {
	    if (check_filename(filename))
		    printk("%s filename = %s\n", __FUNCTION__, filename);
		kfree(filename);
	}

	procname = (char *)ak_get_procname();
    if (procname)
    {
        if (check_procname(procname))
            printk("%s procname = %s\n", __FUNCTION__, procname);
        kfree(procname);
    }

    return 0;
}
int ak360_inode_rename(struct inode *old_inode,
                      struct dentry *old_dentry,
                      struct inode *new_inode,
                      struct dentry *new_dentry)
{
    char *oldfilename = NULL;
    char *newfilename = NULL;
    char *procname = NULL;
    
    struct path oldpath = {
        .dentry = old_dentry,
        .mnt = NULL,
    };

    struct path newpath = {
        .dentry = new_dentry,
        .mnt = NULL,
    };
    
    oldfilename = ak_get_realpath(&oldpath);
	if (oldfilename)
	{
	    if (check_filename(oldfilename))
		    printk("%s oldfilename = %s\n", __FUNCTION__, oldfilename);
		kfree(oldfilename);
	}

	newfilename = ak_get_realpath(&newpath);
	if (newfilename)
	{
	    if (check_filename(newfilename))
		    printk("%s newfilename = %s\n", __FUNCTION__, newfilename);
		kfree(newfilename);
	}
	
	procname = (char *)ak_get_procname();
    if (procname)
    {
        if (check_procname(procname))
            printk("%s procname = %s\n", __FUNCTION__, procname);
        kfree(procname);
    }

    return 0;
}
int ak360_inode_symlink(struct inode *inode, struct dentry *dentry, const char *old_name)
{
    struct path path = {
        .dentry = dentry,
        .mnt = NULL,
    };
    
    char *filename = ak_get_realpath(&path);
	if (filename)
	{
	    if (check_filename(filename))
		    printk("%s filename = %s\n", __FUNCTION__, filename);
		kfree(filename);
	}

    return 0;
}

static int ak360_bprm_check_security(struct linux_binprm *bprm)
{
	char *procname = ak_get_file_realpath(bprm->file);
    if (procname)
    {
        //if (check_procname(procname))
        //printk("%s procname is %s\n", __FUNCTION__, procname);
        kfree(procname);
    }

    if (NULL != bprm)
    {
        //printk("current->fs->pwd = %s\n",current->fs->pwd.mnt->mnt_root->d_name.name);
        //printk("current->fs->root = %s\n",current->fs->root.mnt->mnt_root->d_name.name);
        //printk("bprm->filename is %s\n",bprm->filename);
    }
    return 0;
}

static int ak360_file_permission(struct file *file, int mask)
{
    char *procname = NULL;
	char *filename = ak_get_file_realpath(file);
    if (filename)
    {
        if (check_filename(filename))
            printk("%s filename is %s, mask is %d\n", __FUNCTION__, filename,mask);
        kfree(filename);
    }
    else
    {
        printk("%s get filename failed.\n", __FUNCTION__);
    }
    procname = (char *)ak_get_procname();
    if (procname)
    {
        if (check_procname(procname))
            printk("%s procname = %s\n", __FUNCTION__, procname);
        kfree(procname);
    }

    return 0;
}

static int ak360_file_open(struct file *file, const struct cred *cred)
{
    char *procname = NULL;
    char *filename = ak_get_file_realpath(file);
    if (filename)
    {
        if (check_filename(filename))
            printk("%s filename is %s\n", __FUNCTION__, filename);
        kfree(filename);
    }
    else
    {
        printk("%s get filename failed.\n", __FUNCTION__);
    }
	procname = (char *)ak_get_procname();
    if (procname)
    {
        if (check_procname(procname))
        printk("%s procname = %s\n", __FUNCTION__, procname);
        kfree(procname);
    }

	return 0;
}

#define MY_HOOK_INIT(HEAD, HOOK)				\
	{ .head = &probe_dummy_security_hook_heads.HEAD,	\
			.hook = { .HEAD = HOOK } }

static struct security_hook_list ak360_hooks[] = {
    MY_HOOK_INIT(inode_create, ak360_inode_create),
    MY_HOOK_INIT(inode_mkdir, ak360_inode_mkdir),
    MY_HOOK_INIT(inode_rmdir, ak360_inode_rmdir),
    MY_HOOK_INIT(inode_link, ak360_inode_link),
    MY_HOOK_INIT(inode_unlink, ak360_inode_unlink),
    MY_HOOK_INIT(inode_symlink, ak360_inode_symlink),
    MY_HOOK_INIT(inode_rename, ak360_inode_rename),
    
    MY_HOOK_INIT(bprm_check_security, ak360_bprm_check_security),
    MY_HOOK_INIT(file_permission, ak360_file_permission),
    MY_HOOK_INIT(file_open, ak360_file_open),
};

static inline void add_hook(struct security_hook_list *hook)
{
	list_add_tail_rcu(&hook->list, hook->head);
}

/**
* ccs_init - Initialize this module.
*
* Returns 0 on success, negative value otherwise.
*/

int __init ak_sec_init(void)
{
    int idx;
	struct security_hook_heads *hooks = probe_security_hook_heads();
	if (!hooks)
		goto out;
	for (idx = 0; idx < ARRAY_SIZE(ak360_hooks); idx++)
		ak360_hooks[idx].head = ((void *) hooks)
			+ ((unsigned long) ak360_hooks[idx].head)
			- ((unsigned long) &probe_dummy_security_hook_heads);
	ak_sec_exports.find_task_by_vpid = probe_find_task_by_vpid();
	if (!ak_sec_exports.find_task_by_vpid)
		goto out;
	ak_sec_exports.find_task_by_pid_ns = probe_find_task_by_pid_ns();
	if (!ak_sec_exports.find_task_by_pid_ns)
		goto out;
	ak_sec_exports.d_absolute_path = probe_d_absolute_path();
	if (!ak_sec_exports.d_absolute_path)
		goto out;
    //unsigned int cr0 = clear_and_return_cr0();
	for (idx = 0; idx < ARRAY_SIZE(ak360_hooks); idx++)
		add_hook(&ak360_hooks[idx]);
    //setback_cr0(cr0);
	printk(KERN_INFO
	       "Access Keeping And Regulating Instrument registered.\n");
	return 0;
out:
	return -EINVAL;
}


void ak_sec_exit(void)
{
    //unsigned long cr0 = clear_and_return_cr0();
    security_delete_hooks(ak360_hooks, ARRAY_SIZE(ak360_hooks));
    //setback_cr0(cr0);
    return;
}

