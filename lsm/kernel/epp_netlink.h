#ifndef __EPP_NETLINK_H__
#define __EPP_NETLINK_H__

#include <net/netlink.h>
#include <linux/skbuff.h>
#include <linux/net.h>
#include <net/sock.h>

#include "ak_common.h"

int nelk_init(void);
void nelk_release(void);
int nelk_send(void *data, int datalen);

typedef struct msg_data_s
{
    char szfilename[1024];
    char szpermission[256];
}msg_data_t;

typedef struct msg_permission_s
{
    char szfilename[1024];
    char szpermission[32];
    int permission;
}msg_permission_t;

void set_test_file(msg_data_t *pstdata);

#endif
