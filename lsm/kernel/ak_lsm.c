 /*
  * lsm.c
  *
  * Copyright (C) 2010-2015  sunweifei <sunweifei@360.com.cn>
  *
  * Version: 1.0   2020/3/18
  */


#include <linux/version.h>
#include <linux/security.h>
#include <linux/module.h>
#include <linux/init.h>

#include "epp_netlink.h"




#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 12, 0)
#include "ak_lsm_4.12.c"
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(4, 7, 0)
#include "ak_lsm_4.7.c"
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(4, 2, 0)
#include "ak_lsm_4.2.c"
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 29)
#include "ak_lsm_2.6.29.c"
#else
#include "ak_lsm_2.6.0.c"
#endif


static int __init ak_module_init(void)
{
    printk("<0>""ak_module_init!\n");
    ak_sec_init();
    nelk_init();
    return 0;
}

static void __exit ak_module_exit(void)
{
    printk("<0>""ak_module_exit!\n");
    ak_sec_exit();
    nelk_release();
    #if 0
    mdelay(100);
    if (in_interrupt())
    {
        printk("hook being called from interrupt context\n");
    }
    else
    {
        set_current_state(TASK_INTERRUPTIBLE);
        schedule_timeout(3);
        mdelay(100);
    }
    #endif
}

module_init(ak_module_init);
module_exit(ak_module_exit);

MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
MODULE_AUTHOR("sunweifei");
//MODULE_DESCRIPTION("ak security kernel, build on " __TIME__  " " __DATE__);

