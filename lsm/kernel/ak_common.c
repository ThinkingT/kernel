
#include "ak_common.h"

char* replace(char *buf, int len, char org, char dest)
{
    int cnt=0;
    char *p=buf, *end = buf+len;
    while(p<end){
        if(*p == org){ 
            *p = dest;
            ++cnt;
        }
        p++;
    }
    return buf; 
}

