#include "ak_internal.h"

void __init ak_permission_init(void)
{
#if 1
#if LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 32)
	ak_security_ops.save_open_mode = NULL;
	ak_security_ops.clear_open_mode = NULL;
	ak_security_ops.open_permission = NULL;
#else
	ak_security_ops.open_permission = NULL;
#endif
	ak_security_ops.fcntl_permission = NULL;
	ak_security_ops.ioctl_permission = NULL;
	ak_security_ops.chmod_permission = NULL;
	ak_security_ops.chown_permission = NULL;
#ifdef CONFIG_CCSECURITY_FILE_GETATTR
	ak_security_ops.getattr_permission = NULL;
#endif
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 25)
	ak_security_ops.pivot_root_permission = NULL;
	ak_security_ops.chroot_permission = NULL;
#else
	ak_security_ops.pivot_root_permission = NULL;
	ak_security_ops.chroot_permission = NULL;
#endif
	ak_security_ops.umount_permission = NULL;
	ak_security_ops.mknod_permission = NULL;
	ak_security_ops.mkdir_permission = NULL;
	ak_security_ops.rmdir_permission = NULL;
	ak_security_ops.unlink_permission = NULL;
	ak_security_ops.symlink_permission = NULL;
	ak_security_ops.truncate_permission = NULL;
	ak_security_ops.rename_permission = NULL;
	ak_security_ops.link_permission = NULL;
#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 30)
	ak_security_ops.open_exec_permission = NULL;
	ak_security_ops.uselib_permission = NULL;
#endif
#if LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 18) || (LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 33) && defined(CONFIG_SYSCTL_SYSCALL))
	ak_security_ops.parse_table = NULL;
#endif
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 25)
	ak_security_ops.mount_permission = NULL;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 2, 0)
	ak_security_ops.move_mount_permission = NULL;
#endif
#else
	ak_security_ops.mount_permission = NULL;
#endif
#ifdef CONFIG_CCSECURITY_CAPABILITY
	ak_security_ops.socket_create_permission =
		NULL;
#endif
#ifdef CONFIG_CCSECURITY_NETWORK
	ak_security_ops.socket_listen_permission =
		NULL;
	ak_security_ops.socket_connect_permission =
		NULL;
	ak_security_ops.socket_bind_permission = NULL;
	ak_security_ops.socket_post_accept_permission =
		NULL;
	ak_security_ops.socket_sendmsg_permission =
		NULL;
#endif
#ifdef CONFIG_CCSECURITY_NETWORK_RECVMSG
	ak_security_ops.socket_post_recvmsg_permission =
		NULL;
#endif
#ifdef CONFIG_CCSECURITY_IPC
	ak_security_ops.kill_permission = NULL;
	ak_security_ops.tgkill_permission = NULL;
	ak_security_ops.tkill_permission = NULL;
	ak_security_ops.sigqueue_permission = NULL;
	ak_security_ops.tgsigqueue_permission = NULL;
#endif
#ifdef CONFIG_CCSECURITY_CAPABILITY
	ak_security_ops.capable = NULL;
	ak_security_ops.ptrace_permission = NULL;
#endif
	ak_security_ops.search_binary_handler = NULL;
#else
#if LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 32)
	ak_security_ops.save_open_mode = __ccs_save_open_mode;
	ak_security_ops.clear_open_mode = __ccs_clear_open_mode;
	ak_security_ops.open_permission = __ccs_open_permission;
#else
	ak_security_ops.open_permission = ccs_new_open_permission;
#endif
	ak_security_ops.fcntl_permission = __ccs_fcntl_permission;
	ak_security_ops.ioctl_permission = __ccs_ioctl_permission;
	ak_security_ops.chmod_permission = __ccs_chmod_permission;
	ak_security_ops.chown_permission = __ccs_chown_permission;
#ifdef CONFIG_CCSECURITY_FILE_GETATTR
	ak_security_ops.getattr_permission = __ccs_getattr_permission;
#endif
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 25)
	ak_security_ops.pivot_root_permission = __ccs_pivot_root_permission;
	ak_security_ops.chroot_permission = __ccs_chroot_permission;
#else
	ak_security_ops.pivot_root_permission = ccs_old_pivot_root_permission;
	ak_security_ops.chroot_permission = ccs_old_chroot_permission;
#endif
	ak_security_ops.umount_permission = __ccs_umount_permission;
	ak_security_ops.mknod_permission = __ccs_mknod_permission;
	ak_security_ops.mkdir_permission = __ccs_mkdir_permission;
	ak_security_ops.rmdir_permission = __ccs_rmdir_permission;
	ak_security_ops.unlink_permission = __ccs_unlink_permission;
	ak_security_ops.symlink_permission = __ccs_symlink_permission;
	ak_security_ops.truncate_permission = __ccs_truncate_permission;
	ak_security_ops.rename_permission = __ccs_rename_permission;
	ak_security_ops.link_permission = __ccs_link_permission;
#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 30)
	ak_security_ops.open_exec_permission = __ccs_open_exec_permission;
	ak_security_ops.uselib_permission = __ccs_uselib_permission;
#endif
#if LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 18) || (LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 33) && defined(CONFIG_SYSCTL_SYSCALL))
	ak_security_ops.parse_table = __ccs_parse_table;
#endif
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 25)
	ak_security_ops.mount_permission = __ccs_mount_permission;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 2, 0)
	ak_security_ops.move_mount_permission = __ccs_move_mount_permission;
#endif
#else
	ak_security_ops.mount_permission = ccs_old_mount_permission;
#endif
#ifdef CONFIG_CCSECURITY_CAPABILITY
	ak_security_ops.socket_create_permission =
		__ccs_socket_create_permission;
#endif
#ifdef CONFIG_CCSECURITY_NETWORK
	ak_security_ops.socket_listen_permission =
		__ccs_socket_listen_permission;
	ak_security_ops.socket_connect_permission =
		__ccs_socket_connect_permission;
	ak_security_ops.socket_bind_permission = __ccs_socket_bind_permission;
	ak_security_ops.socket_post_accept_permission =
		__ccs_socket_post_accept_permission;
	ak_security_ops.socket_sendmsg_permission =
		__ccs_socket_sendmsg_permission;
#endif
#ifdef CONFIG_CCSECURITY_NETWORK_RECVMSG
	ak_security_ops.socket_post_recvmsg_permission =
		__ccs_socket_post_recvmsg_permission;
#endif
#ifdef CONFIG_CCSECURITY_IPC
	ak_security_ops.kill_permission = ccs_signal_acl;
	ak_security_ops.tgkill_permission = ccs_signal_acl0;
	ak_security_ops.tkill_permission = ccs_signal_acl;
	ak_security_ops.sigqueue_permission = ccs_signal_acl;
	ak_security_ops.tgsigqueue_permission = ccs_signal_acl0;
#endif
#ifdef CONFIG_CCSECURITY_CAPABILITY
	ak_security_ops.capable = __ccs_capable;
	ak_security_ops.ptrace_permission = __ccs_ptrace_permission;
#endif
	ak_security_ops.search_binary_handler = __ccs_search_binary_handler;
#endif
    return 0;
}

