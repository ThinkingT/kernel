#ifndef __LSM_PUBLIC_H__
#define __LSM_PUBLIC_H__

#define OP_JMP_SIZE 5

struct kernsym {
	void *addr; // orig addr
	void *end_addr;
	unsigned long size;
	const char *name;
	bool name_alloc; // whether or not we alloc'd memory for char *name
	u8 orig_start_bytes[OP_JMP_SIZE];
	void *new_addr;
	unsigned long new_size;
	bool found;
	bool hijacked;
	void *run;
};

int find_symbol_address(struct kernsym *, const char *);
unsigned long find_symbol_addr(char *symname);

unsigned long  clear_and_return_cr0(void);
void setback_cr0(unsigned long val);


#endif

