#include <linux/version.h>
#include <linux/security.h>
#include <linux/module.h>
#include <linux/init.h>

#include "epp_public.h"
#include "epp_lsm.h"

#include "ak_probe.h"
#include "lsm_hook.h"
#include "lsm_public.h"

int find_addr(void)
{
    ulong *paddr = NULL;
    ulong hook_address = 0x00UL;

    hook_address = find_symbol_addr("security_hook_heads");
    printk("hook_address is %lx\n", hook_address);

    if(hook_address == 0x00UL || hook_address == (~0x00UL)){
        printk("get security hook address error\n");
        return -1;
    }

    return 0;
}

static int __init epp_module_init(void)
{
    find_addr();
    printk_info("ak_module_init!\n");
    epp_lsm_init();

    return 0;
}

static void __exit epp_module_exit(void)
{
    printk_info("ak_module_exit!\n");
    epp_lsm_exit();
}

module_init(epp_module_init);
module_exit(epp_module_exit);

MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
MODULE_AUTHOR("sunweifei");


