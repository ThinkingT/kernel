#include <linux/version.h>
#include <linux/security.h>
#include <linux/module.h>
#include <linux/init.h>

#include "epp_public.h"
#include "epp_probe.h"
#include "epp_lsm.h"
int __init epp_lsm_init(void)
{
    printk_info("epp_lsm_init\n");
#if (LINUX_VERSION_CODE <= KERNEL_VERSION(4,2,0))

    struct security_operations *ops = probe_security_ops();
    if (!ops)
        goto out;
    printk_info("ops address is %p", ops);
#else
struct security_hook_heads *ops = probe_security_hook_heads();
if (!ops)
    goto out;
printk_info("ops address is %p", ops);

#endif
out:
    return 0;
}

void __exit epp_lsm_exit(void)
{
    printk_info("epp_lsm_exit!\n");
}

