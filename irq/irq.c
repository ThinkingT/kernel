/*************************************************************************
	> File Name: signal.c
	> Author: Thinking
	> Mail: program_code@sohu.com 
	> Created Time: Wed 16 Jun 2021 05:45:46 AM PDT
 ************************************************************************/

#include <linux/kernel.h>			//Need for loglevels (KERN_WARNING KERN_INFO, KERN_EMERG)

#include <linux/module.h>			//Needed for all kernels modules
#include <linux/moduleparam.h>		

#include <linux/cdev.h>
#include <linux/fs.h>				//filp_open 
#include <linux/wait.h>				
#include <linux/poll.h>
#include <linux/sched.h>
#include <linux/slab.h>				//kmalloc

#include <linux/unistd.h>			//sys_call_table
#include <linux/syscalls.h>
#include <linux/string.h>
#include <linux/fs.h>
#include <linux/fdtable.h>
#include <linux/uaccess.h>
#include <linux/rtc.h>

#include <linux/kallsyms.h>
#include <linux/kprobes.h>
#include <linux/sched.h>
#include <linux/time.h>

#include <linux/ctype.h>
#include <linux/device.h>
#include <linux/cdev.h>

#include <asm/siginfo.h>
#include <linux/pid.h>
//#include <linux/sched/signal.h>
#include <linux/pid_namespace.h>

static int __init irq_init(void)
{
	return 0;
}

void __exit irq_exit(void)
{
    return;
}
module_init(irq_init);
module_exit(irq_exit);
MODULE_LICENSE("GPL");
