KVER:=$(shell uname -r)
KDIR:=/lib/modules/$(KVER)/build
PWD:=$(shell pwd)
SYSCRT_INC:= $(PWD)/
EXTRA_CFLAGS += -I${SYSCRT_INC}/


CONFIG_MODULE_SIG=n

ifneq ($(KERNELRELEASE),)
$(info "2nd")


	#dev_pci-obj:= dev_pci.o
	#device-obj:= device.o

	#obj-m += device.o
	#obj-m += device/dev.o 
	#obj-m += device_pci/dev_pci.o 
	#obj-m += akari/ 
	#obj-m += file_operation/
	#obj-m += signal/
	obj-m += hook/
	#obj-m += thr/
	#obj-m += irq/
	#obj-m += netlink_test/
	#obj-m += ky/

else


all:
	$(info "1st")
	$(MAKE) -C $(KDIR) M=$(PWD) modules
clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean

endif
