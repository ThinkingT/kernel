/*************************************************************************
	> File Name: sig.c
	> Author: Thinking
	> Mail: program_code@sohu.com 
	> Created Time: Wed 15 Dec 2021 11:20:06 PM CST
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>
void handler(int s, siginfo_t* info, void* context){

	printf("signalv : %d\n",s);
}

int main(int argc,char* argv[])
{
	
	int count = 0;
	struct sigaction sa;
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = &handler;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGUSR1, &sa, NULL);
	sigaction(SIGUSR2, &sa, NULL);

	while(1)
	{
		printf("app_handle_signal is running count == %d\n", ++count);
		sleep(5 * 1000);
	}
	
 
    return 0;
}
