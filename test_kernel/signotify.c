/*************************************************************************
	> File Name: signotify.c
	> Author: Thinking
	> Mail: program_code@sohu.com 
	> Created Time: Mon 20 Dec 2021 10:56:54 PM CST
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <signal.h>


#define MY_GPIO_NUMBER 4


char gpio_name[MY_GPIO_NUMBER][16] = {

	"/dev/mygpio0",
	"/dev/mygpio1",
	"/dev/mygpio2",
	"/dev/mygpio3",
};

static void signal_handler(int signum, siginfo_t* info, void* context)
{
	printf("signal_handler: signum = %d \n", signum);
	
	printf("signo = %d, code = %d, errno = %d \n",
			info->si_signo,
			info->si_code,
			info->si_errno);
}


int main(int argc,char* argv[])
{
 
	struct sigaction sa;
	int fd = 0;
	int count = 0;
	int pid = getpid();
	if ((fd = open("/dev/mygpio0", O_RDWR | O_NDELAY)) < 0)
	{
		printf("open dev faild! \n");
		return -1;
	}

	printf("open dev success! \n");

	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = &signal_handler;
	sa.sa_flags = SA_SIGINFO;

	sigaction(SIGUSR1, &sa, NULL);
	sigaction(SIGUSR2, &sa, NULL);

	printf("call ioctl. pid = %d \n", pid);

	ioctl(fd, 100, &pid);


	sleep(10);

	close(fd);
 
    return 0;
}
